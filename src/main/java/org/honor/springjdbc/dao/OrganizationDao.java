package org.honor.springjdbc.dao;

import java.util.List;

import javax.sql.DataSource;

import org.honor.springjdbc.domain.Organization;

public interface OrganizationDao {
	public void setDataSource(DataSource ds);
	public boolean create(Organization org);
	public Organization getOrganization(int id);
	public List<Organization> getAllOrganizations();
	public boolean delete(Organization org);
	public boolean update(Organization org);
	public void cleanup();
}
